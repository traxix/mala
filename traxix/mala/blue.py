from functools import partial
import logging

import pydbus
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk, GtkLayerShell


def bluetooth_list():
    devices = bluetooth_list_devices()
    window = Gtk.Window(title="bluetooth")

    GtkLayerShell.init_for_window(window)
    GtkLayerShell.set_layer(window, GtkLayerShell.Layer.OVERLAY)
    box = Gtk.VBox()
    window.add(box)

    def on_button_clicked(device, _):
        if device:
            if device["connected"] is False:
                connect_device(device["address"])
            else:
                disconnect_device(device["address"])

        GLib.idle_add(window.close)

    # Create a button for each item in the list
    for _, device in devices.items():
        button = Gtk.Button(label=device["name"])
        button.connect("clicked", partial(on_button_clicked, device))
        box.pack_start(button, True, True, 0)

    button = Gtk.Button(label="x")
    button.connect("clicked", partial(on_button_clicked, None))
    box.pack_start(button, True, True, 0)

    window.show_all()
    # window.connect("destroy", Gtk.main_quit)
    return window


def bluetooth_list_devices():
    bus = pydbus.SystemBus()
    ada = bus.get("org.bluez", "/")

    devices = {}
    for key, value in ada.GetManagedObjects().items():
        if not key.startswith("/org/bluez/hci0/"):
            continue
        for _, v in value.items():
            dev = v.get("Alias")
            if dev:
                devices[v["Address"]] = {
                    "name": dev,
                    "connected": v.get("Connected", False),
                    "address": v["Address"],
                }
    return devices


def get_bluetooth_device(address):
    bus = pydbus.SystemBus()

    bluez_service = "org.bluez"
    adapter_path = "/org/bluez/hci0"
    device_path = f"{adapter_path}/dev_{address.replace(':', '_')}"

    return bus.get(bluez_service, device_path)


def connect_device(address):
    device = get_bluetooth_device(address)
    try:
        device.Connect()
        logging.info(f"Connected to {address}")
    except Exception as e:
        logging.info(f"Could not connect to {address}: {e}")


def disconnect_device(address):
    device = get_bluetooth_device(address)
    try:
        device.Disconnect()
        logging.info(f"Connected to {address}")
    except Exception as e:
        logging.info(f"Could not connect to {address}: {e}")
