from enum import Enum

from gi.repository import Gtk


class BatteryState(Enum):
    discharging = "Discharging"
    full = "Full"
    charging = "Charging"
    unknown = "unknown"


class Battery(Gtk.ProgressBar):
    def __init__(self, notifier, alert_threshold=21):
        super().__init__()
        self.set_size_request(30, -1)
        self.notifier = notifier
        self.alert_threshold = alert_threshold
        self.update()

    def update(self):

        with open(
            "/sys/class/power_supply/BAT0/charge_full", "r", encoding="utf-8"
        ) as full, open(
            "/sys/class/power_supply/BAT0/charge_now", "r", encoding="utf-8"
        ) as now:
            level = 100 * float(now.read()) / float(full.read())

        with open("/sys/class/power_supply/BAT0/status") as status:
            bat_state = status.read().strip()

        battery_states = {
            "Discharging": {"icon": "⛈"},
            "Charging": {"icon": "⚡"},
            "Full": {"icon": "💯"},
        }

        try:
            icon = battery_states[bat_state]["icon"]
        except KeyError:
            icon = "?"
            bat_state = "unknown"

        context = self.get_style_context()
        if battery_states not in ["Charging", "Full"] and level < self.alert_threshold:
            context.add_class("alert")
            self.notifier.update(origin="battery", text=f"{level}")

        self.set_fraction(level / 100)
        if level != 100:
            self.set_text(f"{str(int(level))} {icon}")
        else:
            self.set_text(icon)
        self.set_show_text(True)
