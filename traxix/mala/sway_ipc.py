import json
import os
import socket
import struct
import threading


class SwayIPC:
    _MAGIC = "i3-ipc"
    _struct_header = "=%dsII" % len(_MAGIC.encode("utf-8"))
    _struct_header_size = struct.calcsize(_struct_header)

    def __init__(self, socket_path=None):
        if socket_path is None:
            socket_path = os.environ["SWAYSOCK"]

        self.thread = None
        SUBSCRIBE = 2
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.socket.connect(socket_path)
        subscribe_message = self._pack(
            SUBSCRIBE, ["workspace", "mode", "barconfig_update", "bar_state_update"]
        )

        self.socket.sendall(subscribe_message)

        self.read_message()

    @staticmethod
    def _pack(msg_type, payload):
        pb = json.dumps(payload).encode("utf-8")
        s = struct.pack("=II", len(pb), 2)
        return SwayIPC._MAGIC.encode("utf-8") + s + pb

    @staticmethod
    def _unpack_header(data):

        return struct.unpack(
            SwayIPC._struct_header, data[: SwayIPC._struct_header_size]
        )

    @staticmethod
    def _unpack(data):
        msg_magic, msg_length, msg_type = SwayIPC._unpack_header(data)
        msg_size = SwayIPC._struct_header_size + msg_length

        payload = data[SwayIPC._struct_header_size : msg_size]
        return payload.decode("utf-8", "replace")

    def read_message(self):
        data = self.socket.recv(14)

        msg_magic, msg_size, msg_type = self._unpack_header(data)

        while len(data) < msg_size:
            data += self.socket.recv(msg_size)

        payload = self._unpack(data)
        return payload

    def _run(self, callback):
        while payload := self.read_message():
            callback(payload)

    def run(self, callback):
        self.thread = threading.Thread(target=self._run, args=(callback,))
        self.thread.start()
