import logging
import urllib.request
from vbar import VBar
import time


def http_ping(url="https://www.orange.ma"):
    start = time.time()
    response = urllib.request.urlopen(url)
    _ = response.read(1)
    stop = time.time()
    logging.info(f"http_ping> {response.status}, {stop - start}")

    return stop - start


class Ping(VBar):
    def __init__(self):
        super().__init__()
        self.update()

    def update(self):
        self.add(http_ping())
