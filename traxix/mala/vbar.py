from gi.repository import Gtk


class VBar(Gtk.Label):
    BARS = [" ", "_", "▁", "▂", "▃", "▄", "▆", "▇", "█"]

    def __init__(self, size=8):
        super().__init__()
        self.data = [0] * size
        self.index = size - 1

    def add(self, point):
        self.data[self.index] = point
        self.index = (self.index - 1) % len(self.data)

        upper = max(self.data)
        lower = min(self.data)

        spread = upper - lower

        text = "".join(
            [
                VBar.BARS[int(((len(VBar.BARS) - 1) * (item - lower)) / spread)]
                for item in self.data
            ]
        )
        self.set_text(f"{text} {int(1000*point)}")
