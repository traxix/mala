#!/home/trax/venv/all/bin/python3

import json
import logging
from datetime import datetime
from enum import Enum

import gi
from blue import bluetooth_list
from css import CSS
from fire import Fire
from sway_ipc import SwayIPC

gi.require_version("Gtk", "3.0")

from battery import Battery
from notification import Notification
from ping import Ping

gi.require_version("GtkLayerShell", "0.1")
from gi.repository import Gdk, GLib, Gtk, GtkLayerShell


class Severity(Enum):
    normal = 0
    warning = 1
    critical = 2


class Mala:
    def __init__(self):
        self.notifier = Notification()
        self.window = Gtk.Window()
        self.window.set_size_request(-1, 10)
        self.clock = Gtk.Label()
        self.clock.set_size_request(200, -1)

        blue = Gtk.Label(label="🪥")
        event_box = Gtk.EventBox()
        event_box.add(blue)
        self.ping = Ping()

        event_box.connect("button-press-event", self.blue_clicked)

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=1)
        box.pack_start(self.clock, True, True, 0)

        self.battery = Battery(notifier=self.notifier)

        box.pack_end(self.battery, False, False, 0)
        box.pack_end(event_box, False, False, 0)
        box.pack_end(self.ping, False, False, 0)

        # window.pack_start(box, True, True, 0)
        self.window.add(box)

        GtkLayerShell.init_for_window(self.window)
        # GtkLayerShell.auto_exclusive_zone_enable(self.window)
        GtkLayerShell.set_layer(self.window, GtkLayerShell.Layer.OVERLAY)

        GtkLayerShell.set_margin(self.window, GtkLayerShell.Edge.TOP, 0)
        GtkLayerShell.set_margin(self.window, GtkLayerShell.Edge.LEFT, 0)
        GtkLayerShell.set_margin(self.window, GtkLayerShell.Edge.RIGHT, 0)

        GtkLayerShell.set_anchor(self.window, GtkLayerShell.Edge.TOP, True)
        GtkLayerShell.set_anchor(self.window, GtkLayerShell.Edge.LEFT, True)
        GtkLayerShell.set_anchor(self.window, GtkLayerShell.Edge.RIGHT, True)

        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(CSS)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

        self.window.show_all()

        self.window.connect("destroy", Gtk.main_quit)

        self.update_time_slow()
        GLib.timeout_add_seconds(1, self.update_time_quick)
        GLib.timeout_add_seconds(10, self.update_time_slow)

        logging.info("started thread")
        sway_ipc = SwayIPC()
        sway_ipc.run(self.sway_event)
        self.window.hide()
        Gtk.main()
        logging.info("bye bye")

    def blue_clicked(self, _, __):
        bluetooth_list()

    def update_time_quick(self):
        current_time = datetime.now().strftime("%H:%M:%S")
        self.clock.set_text(current_time)
        return True

    def update_time_slow(self):
        logging.debug("slow treatment")
        self.battery.update()
        self.ping.update()
        return True

    def sway_event(self, event):
        logging.debug(f"Sway event: {event}")
        event = json.loads(event)
        if "visible_by_modifier" in event:
            if event["visible_by_modifier"]:
                GLib.idle_add(self.window.show)
            else:
                GLib.idle_add(self.window.hide)


if __name__ == "__main__":
    Fire(Mala)
