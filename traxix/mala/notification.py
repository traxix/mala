import logging
from datetime import datetime

from css import CSS
from gi.repository import Gdk, Gtk, GtkLayerShell


class Notification:
    def __init__(self):

        self.debounce = {}
        self.window = Gtk.Window()
        self.window.set_size_request(-1, 10)
        self.label = Gtk.Label()
        self.label.set_size_request(200, -1)
        self.label.set_text("")

        button = Gtk.Button(label="X")
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=1)
        box.pack_start(self.label, True, True, 0)
        box.pack_end(button, False, False, 0)

        button.connect("clicked", self.notification_close)

        self.window.add(box)

        GtkLayerShell.init_for_window(self.window)
        GtkLayerShell.set_layer(self.window, GtkLayerShell.Layer.OVERLAY)

        GtkLayerShell.set_anchor(self.window, GtkLayerShell.Edge.TOP, True)
        GtkLayerShell.set_anchor(self.window, GtkLayerShell.Edge.LEFT, True)
        GtkLayerShell.set_anchor(self.window, GtkLayerShell.Edge.RIGHT, True)

        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(CSS)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

        self.window.show_all()

        logging.info("bye bye")
        self.window.hide()

    def notification_close(self, button):
        self.window.hide()

    def update(self, origin, text):
        if data := self.debounce.get(origin):
            delta = (datetime.now() - data["time"]).seconds
            data["text"] = text
            if delta < 120:
                return
            else:
                data["time"] = datetime.now()
        else:
            self.debounce[origin] = {"time": datetime.now(), "text": text}

        message = ""
        for key, value in self.debounce.items():
            message += f'[{key}] {value["text"]} '

        self.label.set_text(message)
        self.window.show()
